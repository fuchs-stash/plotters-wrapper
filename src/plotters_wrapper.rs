use std::time::Duration;

use plotters::{
    backend::SVGBackend,
    chart::ChartBuilder,
    coord::{combinators::IntoLinspace, ranged1d::Ranged},
    drawing::IntoDrawingArea,
    element::{Circle, EmptyElement},
    series::{AreaSeries, LineSeries, PointSeries},
    style::{Color, ShapeStyle, RED, WHITE},
};

pub fn create_line_plot(
    filename: impl Into<String>,
    title: impl Into<String>,
    x_desc: impl Into<String>,
    y_desc: impl Into<String>,
    img_size: (u32, u32),
    values: Vec<(u64, u64)>,
) {
    let filename = filename.into();
    let title = title.into();

    let root_area = SVGBackend::new(&filename, img_size).into_drawing_area();

    let start_x = values.first().unwrap().0;
    let end_x = values.last().unwrap().0;

    let min_y = values
        .iter()
        .min_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    let max_y = values
        .iter()
        .max_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    root_area.fill(&WHITE).unwrap();

    let x_axis = (start_x..end_x).step(1);

    let mut cc = ChartBuilder::on(&root_area)
        .margin(5)
        .set_all_label_area_size(50)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_axis.range(), min_y..max_y)
        .unwrap();

    cc.configure_mesh()
        .x_labels(12)
        .y_labels(10)
        .max_light_lines(10)
        .x_label_formatter(&|v| Duration::from_millis(*v).as_secs().to_string())
        .y_label_formatter(&|v| v.to_string())
        .y_desc(y_desc)
        .x_desc(x_desc)
        .draw()
        .unwrap();

    cc.draw_series(LineSeries::new(
        values.iter().map(|data| (data.0, data.1)),
        &RED,
    ))
    .unwrap();

    root_area.present().unwrap();
}

pub fn create_point_plot(
    filename: impl Into<String>,
    title: impl Into<String>,
    img_size: (u32, u32),
    values: Vec<(u64, u64)>,
) {
    let filename = filename.into();
    let title = title.into();

    let root_area = SVGBackend::new(&filename, img_size).into_drawing_area();

    let start_x = values.first().unwrap().0;
    let end_x = values.last().unwrap().0;

    let min_y = values
        .iter()
        .min_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    let max_y = values
        .iter()
        .max_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    root_area.fill(&WHITE).unwrap();

    let x_axis = (start_x..end_x).step(1);

    let mut cc = ChartBuilder::on(&root_area)
        .margin(5)
        .set_all_label_area_size(50)
        .caption(title, ("sans-serif", 30))
        .build_cartesian_2d(x_axis.range(), min_y..max_y)
        .unwrap();

    cc.configure_mesh()
        .x_labels(12)
        .y_labels(10)
        .max_light_lines(10)
        .x_label_formatter(&|v| Duration::from_millis(*v).as_secs().to_string())
        .y_label_formatter(&|v| v.to_string())
        .draw()
        .unwrap();

    cc.draw_series(PointSeries::of_element(
        values,
        1,
        ShapeStyle::from(&RED).filled(),
        &|coord, size, style| {
            EmptyElement::at(coord) + Circle::new((0, 0), size, style)
            // + plotters::element::Text::new(format!("{:?}", coord), (0, 15), ("sans-serif", 15))
        },
    ))
    .unwrap();

    root_area.present().unwrap();
}

pub fn create_area_plot(
    filename: impl Into<String>,
    title: impl Into<String>,
    img_size: (u32, u32),
    values: Vec<(u64, u64)>,
) {
    let filename = filename.into();
    let title = title.into();

    let root_area = SVGBackend::new(&filename, img_size).into_drawing_area();

    let start_x = values.first().unwrap().0;
    let end_x = values.last().unwrap().0;

    let min_y = values
        .iter()
        .min_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    let max_y = values
        .iter()
        .max_by(|elem1, elem2| elem1.1.cmp(&elem2.1))
        .unwrap()
        .1;

    root_area.fill(&WHITE).unwrap();

    let x_axis = (start_x..end_x).step(1);

    let mut cc = ChartBuilder::on(&root_area)
        .margin(5)
        .set_all_label_area_size(50)
        .caption(title, ("sans-serif", 40))
        .build_cartesian_2d(x_axis.range(), min_y..max_y)
        .unwrap();

    cc.configure_mesh()
        .x_labels(12)
        .y_labels(10)
        .max_light_lines(10)
        .x_label_formatter(&|v| v.to_string())
        .y_label_formatter(&|v| v.to_string())
        .draw()
        .unwrap();

    cc.draw_series(AreaSeries::new(values, 0, RED.mix(0.2)).border_style(RED))
        .unwrap();

    root_area.present().unwrap();
}
